package edu.ntnu.idatt1002;

import java.util.ArrayList;
/**
 * Represents a hand of playing cards. A hand of playing cards contains 5 playing cards.
 *
 * @author Oskar Remvang
 * @version 2021-04-07
 */
public class HandOfCards {

    ArrayList<PlayingCard> cards;

    /**
     * Creates an instance of a HandOfCards.
     * Picks 5 random cards from a DeckOfCards and assigns field cards to it
     */
    public HandOfCards(){
        DeckOfCards deck = new DeckOfCards();
        cards = deck.dealHand(5);
    }

    /**
     * Returns all cards in the hand as a string
     * @return String
     */
    public String getCardsAsString(){
        String cardsAsString = "";
        for(PlayingCard card : cards){
            cardsAsString += card.getAsString() + " ";
        }
        return  cardsAsString;
    }

    /**
     * Sums all the faces of the cards in the hand
     * @return int
     */
    public int getSum(){
        return cards.stream().map(PlayingCard::getFace).reduce(0,(a, b)->a+b);
    }

    /**
     * Returns all playing cards of the suit hearts as a single string
     * @return String
     */
    public String getHearts(){
        return cards.stream().filter(card -> card.getSuit()=='H').map(PlayingCard::getAsString).reduce("",(a,b)-> a+" "+b);
    }

    /**
     * Checks if the hand contains a queen of spades or not
     * @return Boolean
     */
    public boolean containsQueenOfSpades(){
        return cards.stream().anyMatch(card -> card.getAsString().equals("S12"));
    }

    /**
     * Checks if the hand contains five cards of the same suit(a flush) or not
     * @return Boolean
     */
    public boolean isFlush(){
        return cards.stream().allMatch(c -> c.getSuit() == 'H') ||
                cards.stream().allMatch(c -> c.getSuit() == 'S') ||
                cards.stream().allMatch(c -> c.getSuit() == 'C') ||
                cards.stream().allMatch(c -> c.getSuit() == 'D');
    }

    /**
     * Method for testing only
     * @param newHand
     */
    void setHand(ArrayList<PlayingCard> newHand){
        cards = newHand;
    }
}
