package edu.ntnu.idatt1002;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a deck of playing cards. A deck of playing cards has 52 different playing cards.
 *
 * @author Oskar Remvang
 * @version 2021-04-07
 */
public class DeckOfCards {

    private ArrayList<PlayingCard> deck;
    private final char[] suit = { 'S', 'H', 'D', 'C' };

    /**
     * Creates an instance of a DeckOfCards.
     * Instanciates and fills an ArrayList<PlayingCard> deck.
     */
    public DeckOfCards(){

        this.deck = new ArrayList<>(52);

        for(char suit : suit){
            for(int i = 1; i <= 13; i++){
                PlayingCard card = new PlayingCard(suit,i);
                deck.add(card);
            }
        }
    }

    /**
     * Returns an ArrayList<PlayingCard> filled with n random playing cards picked out from the deck
     * @param n
     * @return ArrayList<PlayingCard>
     */
    public ArrayList<PlayingCard> dealHand(int n){

        Random random = new Random();

        ArrayList<PlayingCard> returnCards = new ArrayList<>(n);
        for(int i = 0; i < n; i++){
            PlayingCard card = deck.get(random.nextInt(51-i));
            returnCards.add(card);
        }

        return returnCards;
    }
}
