package edu.ntnu.idatt1002;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    private Text cards;
    private Text sum;
    private Text cardsOfHearts;
    private Text flush;
    private Text queenOfSpades;
    private HandOfCards hand;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Card Game");
        stage.setResizable(false);

        Group root = new Group();
        Scene scene = new Scene(root,650,450);

        cards = new Text();
        cards.setY(100);
        cards.setX(200);
        cards.setFont(Font.font("Verdana",30));

        Button dealHand = new Button("Deal Hand");
        dealHand.setLayoutX(150);
        dealHand.setLayoutY(200);
        dealHand.setOnAction(event -> {
            hand = new HandOfCards();
            cards.setText(hand.getCardsAsString());
        });

        Button checkHand = new Button("Check Hand");
        checkHand.setLayoutX(400);
        checkHand.setLayoutY(200);
        checkHand.setOnAction(event -> {
            sum.setText(Integer.toString(hand.getSum()));
            cardsOfHearts.setText(hand.getHearts());
            flush.setText(Boolean.toString(hand.isFlush()));
            queenOfSpades.setText(Boolean.toString(hand.containsQueenOfSpades()));
        });

        Text sumTitle = new Text("Sum of the faces:");
        sumTitle.setY(300);
        sumTitle.setX(100);

        sum = new Text();
        sum.setY(300);
        sum.setX(200);

        Text cardsOfHeartsTitle = new Text("Cards of hearts:");
        cardsOfHeartsTitle.setY(300);
        cardsOfHeartsTitle.setX(350);

        cardsOfHearts = new Text();
        cardsOfHearts.setY(300);
        cardsOfHearts.setX(450);

        Text flushTitle = new Text("Flush:");
        flushTitle.setY(380);
        flushTitle.setX(100);

        flush = new Text();
        flush.setY(380);
        flush.setX(150);

        Text queenOfSpadesTitle = new Text("Queen of spades:");
        queenOfSpadesTitle.setY(380);
        queenOfSpadesTitle.setX(350);

        queenOfSpades = new Text();
        queenOfSpades.setY(380);
        queenOfSpades.setX(450);

        root.getChildren().add(cards);
        root.getChildren().add(dealHand);
        root.getChildren().add(checkHand);
        root.getChildren().add(sumTitle);
        root.getChildren().add(sum);
        root.getChildren().add(cardsOfHeartsTitle);
        root.getChildren().add(cardsOfHearts);
        root.getChildren().add(flushTitle);
        root.getChildren().add(flush);
        root.getChildren().add(queenOfSpadesTitle);
        root.getChildren().add(queenOfSpades);
        stage.setScene(scene);
        stage.show();
    }
}
