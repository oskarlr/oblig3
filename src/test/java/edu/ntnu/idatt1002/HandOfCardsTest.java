package edu.ntnu.idatt1002;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    @Test
    void getSum() {
        HandOfCards hand = new HandOfCards();
        int sum = 0;
        for(PlayingCard card : hand.cards){
            sum += card.getFace();
        }
        Assertions.assertTrue(hand.getSum()==sum);
    }

    @Test
    void getHearts() {
        HandOfCards hand = new HandOfCards();
        PlayingCard heart1 = new PlayingCard('H',10);
        PlayingCard heart2 = new PlayingCard('H',8);
        PlayingCard heart3 = new PlayingCard('H',6);
        PlayingCard spade = new PlayingCard('S',4);
        ArrayList<PlayingCard> newHand = new ArrayList<>();
        newHand.add(heart1);
        newHand.add(heart2);
        newHand.add(heart3);
        newHand.add(spade);
        hand.setHand(newHand);
        String hearts = " H10 H8 H6";
        Assertions.assertEquals(hearts,hand.getHearts());
    }

    @Test
    void containsQueenOfSpades() {
        HandOfCards hand = new HandOfCards();
        PlayingCard heart1 = new PlayingCard('H',10);
        PlayingCard heart2 = new PlayingCard('H',8);
        PlayingCard heart3 = new PlayingCard('H',6);
        PlayingCard queenOfSpade = new PlayingCard('S',12);
        ArrayList<PlayingCard> newHand = new ArrayList<>();
        newHand.add(heart1);
        newHand.add(heart2);
        newHand.add(heart3);
        newHand.add(queenOfSpade);
        hand.setHand(newHand);
        Assertions.assertTrue(hand.containsQueenOfSpades());
    }

    @Test
    void doesNotContainQueenOfSpades() {
        HandOfCards hand = new HandOfCards();
        PlayingCard heart1 = new PlayingCard('H',10);
        PlayingCard heart2 = new PlayingCard('H',8);
        PlayingCard heart3 = new PlayingCard('H',6);
        ArrayList<PlayingCard> newHand = new ArrayList<>();
        newHand.add(heart1);
        newHand.add(heart2);
        newHand.add(heart3);
        hand.setHand(newHand);
        Assertions.assertFalse(hand.containsQueenOfSpades());
    }

    @Test
    void isFlush() {
        HandOfCards hand = new HandOfCards();
        PlayingCard heart1 = new PlayingCard('H',10);
        PlayingCard heart2 = new PlayingCard('H',8);
        PlayingCard heart3 = new PlayingCard('H',6);
        PlayingCard heart4 = new PlayingCard('H',4);
        PlayingCard heart5 = new PlayingCard('H',2);
        ArrayList<PlayingCard> newHand = new ArrayList<>();
        newHand.add(heart1);
        newHand.add(heart2);
        newHand.add(heart3);
        newHand.add(heart4);
        newHand.add(heart5);
        hand.setHand(newHand);
        Assertions.assertTrue(hand.isFlush());
    }

    @Test
    void isNotFlush() {
        HandOfCards hand = new HandOfCards();
        PlayingCard heart1 = new PlayingCard('H',10);
        PlayingCard heart2 = new PlayingCard('H',8);
        PlayingCard heart3 = new PlayingCard('H',6);
        PlayingCard heart4 = new PlayingCard('H',4);
        PlayingCard heart5 = new PlayingCard('S',2);
        ArrayList<PlayingCard> newHand = new ArrayList<>();
        newHand.add(heart1);
        newHand.add(heart2);
        newHand.add(heart3);
        newHand.add(heart4);
        newHand.add(heart5);
        hand.setHand(newHand);
        Assertions.assertFalse(hand.isFlush());
    }
}