package edu.ntnu.idatt1002;

import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    @org.junit.jupiter.api.Test
    void dealHand() {
        DeckOfCards deck = new DeckOfCards();
        ArrayList<PlayingCard> cards = deck.dealHand(5);
        Assertions.assertTrue(cards.size()==5);
        System.out.println(cards);
        cards = deck.dealHand(3);
        Assertions.assertTrue(cards.size()==3);
        System.out.println(cards);
    }
}